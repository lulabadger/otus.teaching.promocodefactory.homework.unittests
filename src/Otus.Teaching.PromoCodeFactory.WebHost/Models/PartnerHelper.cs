﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PartnerHelper
    {
        public static ActionResult<Partner> Validate(Partner partner, bool needCheckActive = false)
        {
            if (partner == null)
                return new Microsoft.AspNetCore.Mvc.NotFoundResult();

            if (needCheckActive)
            {
                if (!partner.IsActive)
                    return new Microsoft.AspNetCore.Mvc.BadRequestObjectResult("Данный партнер не активен");
            }
            return new OkObjectResult(partner);
        }

        /// <summary>
        /// Добавление лимита партнеру
        /// </summary>
        /// <param name="request">запрос на добавлние</param>
        /// <param name="partner">партнер</param>
        /// <returns>Добавленный лимит</returns>
        public static PartnerPromoCodeLimit SetLimit(Partner partner, SetPartnerPromoCodeLimitRequest request)
        {
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                            !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }
            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = request.EndDate
            };

            partner.PartnerLimits.Add(newLimit);

            return newLimit;
        }

    }
}
