﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using System;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions.Extensions;
using FluentAssertions.Execution;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            Partner partner = null;
            Guid _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId)).ReturnsAsync(partner);

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotActive_ReturnsBadRequest()
        {

            // Arrange
            var partner = new PartnerBuilder()
                .WithCreatedActive(false)
                .Build();
            Guid _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId)).ReturnsAsync(partner);

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>("Данный партнер не активен");
        }


        /// <summary>
        /// Если партнеру выставляется лимит а предыдущий не отменен, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RequestLimitMoreThen0AndExistingLimitIsNotCanceled_NumberIssuedPromoCodesReset()
        {
            // Arrange            
            var partner = new PartnerBuilder()
               .WithCreatedCanceledDateTime(null)
               .Build();
            Guid _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert 
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, который закончился, то количество NumberIssuedPromoCodes не обнуляется
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RequestLimitMoreThen0AndExistingLimitIsCanceled_NumberIssuedPromoCodesIsNotReset()
        {
            // Arrange
            int excpectedNumberIssuedPromoCodes = 150;

            var partner = new PartnerBuilder()
               .WithCreatedNumberIssuedPromoCodes(excpectedNumberIssuedPromoCodes)
               .WithCreatedCanceledDateTime(DateTime.Now.AddDays(-1))
               .Build();

            Guid _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert 
            partner.NumberIssuedPromoCodes.Should().Be(excpectedNumberIssuedPromoCodes);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RequestLimitMoreThen0AndExistingLimitNotCanceled_NumberIssuedPromoCodesIsNotReset()
        {
            // Arrange          
            var partner = new PartnerBuilder()
               .WithCreatedCanceledDateTime(null)
               .Build();

            Guid _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            DateTime excpectedAfterDateTime = DateTime.Now;
            var exceptedExistingLimit = partner.PartnerLimits.First();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            exceptedExistingLimit.CancelDate.Should()
                .BeAfter(excpectedAfterDateTime)
                .And
                .BeBefore(DateTime.Now);

        }

        /// <summary>
        ///Лимит должен быть больше 0
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RequestLimitLess0_ReturnsBadRequest()
        {
            // Arrange
            var partner = new PartnerBuilder()
               .WithCreatedActive(true)
               .Build();

            Guid _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            var request = new Fixture()
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(r => r.Limit, -20)
                .Create();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>("Лимит должен быть больше 0");
        }


        /// <summary>
        ///Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом)
        ///В Arrange ServiceProvider создается из TestFixture при выполнении теста.
        /// </summary>        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RequestLimitIs550_LimitSaveToDataBase()
        {
            // Arrange
            SetPartnerPromoCodeLimitAsyncTests_IOC ioc = new SetPartnerPromoCodeLimitAsyncTests_IOC();
            var partnersRepository = ioc.ServiceProvider.GetService<IRepository<Partner>>();
            var partnersController = new PartnersController(partnersRepository);

            Guid _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            var partner = new PartnerBuilder()
              .WithCreatedPartnerId(_partnerId)
              .Build();

            await partnersRepository.AddAsync(partner);

            int excpectedLimit = 550;
            var request = new Fixture()
                           .Build<SetPartnerPromoCodeLimitRequest>()
                           .With(r => r.Limit, excpectedLimit)
                           .Create();
            // Act
            var resultSet = await partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            resultSet.Should().NotBeAssignableTo<BadRequestObjectResult>();
            var resultUpdateLimit = await partnersRepository.GetAllAsync();
            var updatePartner = resultUpdateLimit.FirstOrDefault(x => x.Id == _partnerId);
            updatePartner.Should().NotBeNull();
            updatePartner.PartnerLimits.Should().NotBeNull();
            updatePartner.PartnerLimits.Should().HaveCount(2);
            updatePartner.PartnerLimits.Should().Contain(x => x.Limit == excpectedLimit);

        }

        /// <summary>
        ///+1:
        ///Если лимит меньше 0, то не нужно обнулять количество промокодов NumberIssuedPromoCodes и не нужно отменять действующий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_RequestLimitLess0AndExistingLimitIsNotCanceled_NumberIssuedPromoCodesNotReset()
        {
            int excpectedNumberIssuedPromoCodes = 150;

            // Arrange
            var partner = new PartnerBuilder()
               .WithCreatedActive(true)
               .WithCreatedNumberIssuedPromoCodes(excpectedNumberIssuedPromoCodes)
               .WithCreatedCanceledDateTime(null)
               .Build();

            Guid _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            var request = new Fixture()
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(r => r.Limit, -20)
                .Create();


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(excpectedNumberIssuedPromoCodes);
            partner.PartnerLimits.First().CancelDate.Should().BeNull();
        }


    }
}