﻿using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerHelperTests
    {

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void Validate_PartnerIsNotFound_ReturnsNotFound(bool IsActive)
        {
            // Arrange
            Partner partner = null;

            // Act
            var result = PartnerHelper.Validate(partner, IsActive);

            // Assert
            result.Should().NotBeNull();
            result.Result.Should().NotBeNull();
            result.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public void Validate_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = new PartnerBuilder()
                .WithCreatedActive(false)
                .Build();

            // Act
            var result = PartnerHelper.Validate(partner, true);

            // Assert
            result.Should().NotBeNull();
            result.Result.Should().NotBeNull();
            result.Result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнер не заблокирован, то также нужно выдать OK;
        /// </summary>
        [Fact]
        public void Validate_PartnerIsActive_ReturnsOk()
        {
            // Arrange
            var partner = new PartnerBuilder()
                .WithCreatedActive(true)
                .Build();

            // Act
            var result = PartnerHelper.Validate(partner, true);

            // Assert
            result.Should().NotBeNull();
            result.Result.Should().NotBeNull();
            result.Result.Should().BeAssignableTo<OkObjectResult>();
            (result.Result as OkObjectResult).Value.Should().Be(partner);
        }

        /// <summary>
        /// Если партнеру выставляется лимит а предыдущий не отменен, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public  void SetLimit_RequestLimitMoreThen0AndExistingLimitIsNotCanceled_NumberIssuedPromoCodesReset()
        {
            // Arrange            
            var partner = new PartnerBuilder()
               .WithCreatedCanceledDateTime(null)
               .Build();

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            PartnerHelper.SetLimit(partner, request);

            // Assert 
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, который закончился, то количество NumberIssuedPromoCodes не обнуляется
        /// </summary>
        [Fact]
        public  void SetLimit__RequestLimitMoreThen0AndExistingLimitIsCanceled_NumberIssuedPromoCodesIsNotReset()
        {
            // Arrange
            int excpectedNumberIssuedPromoCodes = 150;

            var partner = new PartnerBuilder()
               .WithCreatedNumberIssuedPromoCodes(excpectedNumberIssuedPromoCodes)
               .WithCreatedCanceledDateTime(DateTime.Now.AddDays(-1))
               .Build();            

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            PartnerHelper.SetLimit(partner, request);

            // Assert 
            partner.NumberIssuedPromoCodes.Should().Be(excpectedNumberIssuedPromoCodes);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public void SetLimit_RequestLimitMoreThen0AndExistingLimitNotCanceled_NumberIssuedPromoCodesIsNotReset()
        {
            // Arrange          
            var partner = new PartnerBuilder()
               .WithCreatedCanceledDateTime(null)
               .Build();
            

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            DateTime excpectedAfterDateTime = DateTime.Now;
            var exceptedExistingLimit = partner.PartnerLimits.First();

            // Act
            PartnerHelper.SetLimit(partner, request);

            // Assert
            exceptedExistingLimit.CancelDate.Should()
                .BeAfter(excpectedAfterDateTime)
                .And
                .BeBefore(DateTime.Now);

        }
        
        /// <summary>
        /// При установке лимита новый лимит добавляется партнеру
        /// </summary>
        [Fact]
        public void SetLimit_RequestLimitMoreThen0_LimitAddToPartner()
        {
            // Arrange          
            var partner = new PartnerBuilder()
               .WithCreatedCanceledDateTime(null)
               .Build();            

            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
          
            // Act
           var newLimit= PartnerHelper.SetLimit(partner, request);

            // Assert
            partner.PartnerLimits.Should().Contain(newLimit);
        }

    }
}
