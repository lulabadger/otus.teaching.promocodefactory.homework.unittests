﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost
{

    public class PartnerBuilder
    {
        private bool _isActive = true;
        private int _numberIssuedPromoCodes;        
        private DateTime ?_cancelDate;
        private Guid _partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
        public PartnerBuilder()
        {
        }

        public PartnerBuilder(Guid partnerId)
        {
            WithCreatedPartnerId(partnerId);
        }

        public PartnerBuilder WithCreatedPartnerId(Guid partnerId)
        {
            _partnerId = partnerId;
            return this;
        }
        public PartnerBuilder WithCreatedActive(bool isActive)
        {
            _isActive = isActive;
            return this;
        } 
        
        public PartnerBuilder WithCreatedCanceledDateTime(DateTime? cancelDate)
        {
            _cancelDate = cancelDate;
            return this;
        }      

        public PartnerBuilder WithCreatedNumberIssuedPromoCodes(int numberIssuedPromoCodes)
        {
            _numberIssuedPromoCodes = numberIssuedPromoCodes;
            return this;
        }

        public Partner Build()
        {
            return new Partner()
            {
                Id = _partnerId,
                Name = "Суперигрушки",
                IsActive = _isActive,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100,
                        CancelDate = _cancelDate
                    }
                }
            };
        }
    }

   


}
