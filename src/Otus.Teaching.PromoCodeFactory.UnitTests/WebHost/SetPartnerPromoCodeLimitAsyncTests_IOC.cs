﻿using FluentAssertions.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost
{
   
    public class SetPartnerPromoCodeLimitAsyncTests_IOC : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }

        public IServiceCollection ServiceCollection { get; set; }

        /// <summary>
        /// Выполняется перед запуском тестов
        /// </summary>
        public SetPartnerPromoCodeLimitAsyncTests_IOC()
        {        
            var configurationBuilder = new ConfigurationBuilder();
            var configuration = configurationBuilder.Build();
            ServiceCollection = Configuration.GetServiceCollection(configuration);
            var serviceProvider = GetServiceProvider();
            ServiceProvider = serviceProvider;
        }

        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();
            return serviceProvider;
        }

        public void Dispose()
        {
        }
    }
}
